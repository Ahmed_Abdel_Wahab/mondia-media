package pageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class POM {

	WebDriver driver;


	// public By GamesSelector = By.cssSelector("#main-menu > a:nth-child(2)");  	 
	public By GamesSelector = By.linkText("Games");  

	public By LeftPriceSlider = By.xpath("//*[@id='price-range-slider']/span[1]");

	public By RightPriceSlider = By.xpath("//*[@id='price-range-slider']/span[2]");  

	public String URL= "http://www.konakart.com/konakart/Welcome.action";
	
	
	
	
	public POM(WebDriver driver) {
		this.driver = driver;
	}


}
