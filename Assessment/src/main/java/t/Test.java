package t;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import pageObjectModel.POM;

/**
 * Hello world!
 *
 */


public class Test 
{

	WebDriver driver;
	//Object from page object model'POM' that hold used elements as test case scenario elements factory
	POM objPOM; 

	@org.testng.annotations.BeforeTest


	public void setup() throws InterruptedException{
		//Generic hard coded used driver that's connected to project files wherever it is
		System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		//Driver option to 'not pop-up/headless' "true" or be 'pop-up/not headless' "false"
		options.setHeadless(false);
		driver = new ChromeDriver(options);
		//implicit wait
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		//*****First step in the scenario 'Open website http://www.konakart.com/konakart/Welcome.action.'
		//Object from POM for URL to avoid changing in main script if needed
		driver.get("http://www.konakart.com/konakart/Welcome.action");
		Thread.sleep(400);

	}


	@org.testng.annotations.Test

	public void main() throws InterruptedException
	{
		//Initialization of driver and waiting
		WebDriverWait wait = new WebDriverWait(driver,25000);
		objPOM = new POM(driver);
		//******Second step in the requirement 'Select Games categories.'
		//Wait till element appearance
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.GamesSelector));

		driver.findElement(objPOM.GamesSelector).click();        
		Thread.sleep(2000);
		driver.navigate().refresh();
		Thread.sleep(2000);
		//*****Thrird step in the requirement 'Select Games categories.'
		//Wait till element appearance
		//wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.LeftPriceSlider));
		//driver.switchTo().frame(0); //need to switch to this frame before clicking the slider
		WebElement slider = driver.findElement(objPOM.LeftPriceSlider);
		//import selenium interaction
		Actions move = new Actions(driver);
		//*****4 step in the requirement 'Add the last one to the cart.'
		//change 'second argument value' it to be equivalent the needed value
		Action action = (Action) move.dragAndDropBy(slider, -5, 0).build();
		action.perform();
		Thread.sleep(3000);
		//Wait till element appearance
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.RightPriceSlider));
		WebElement slider1 = driver.findElement(objPOM.RightPriceSlider);
		Actions move1 = new Actions(driver);
		//change 'second argument value' it to be equivalent the needed value
		Action action1 = (Action) move1.dragAndDropBy(slider1, -50, 0).build();
		action1.perform();
		Thread.sleep(500);
		//*****5 step in the requirement 'Add the last one to the cart'
		//Perform cursor maneuver to state container as true displayed
		WebElement link_Home =driver.findElement(By.cssSelector("#item-overview > div.items > ul > li:nth-child(2) > div > img"));
		Actions builder = new Actions(driver);
		Action mouseOverHome = builder
				.moveToElement(link_Home)
				.build();
		mouseOverHome.perform(); 
		Thread.sleep(200);
		//click on add to chart after focusing on item
		driver.findElement(By.xpath("//*[@id='562atc-21']")).click();
		//*****6 step in the requirement 'Verify the item added in the cart without opening the cart page hint 'Use header'.'
		//wait till action recorded and message of adding item is displayed 
		Thread.sleep(300);
		String ShoppingChart=driver.findElement(By.xpath("//*[@id='shopping-cart-items']/div/div")).getText();
		System.out.println(ShoppingChart);
		//Trim caught text based one their index to catch needed combo. of chars 
		String totalMoney = ShoppingChart.substring(1, 6);
		String quantity = ShoppingChart.substring(16, 18);
		//Print data extracted and check i data extracted properly
		System.out.println("Total money :"+totalMoney);
		System.out.println("Items Quantity :"+quantity);	
		if(quantity!= null)
		{
			System.out.println("Successfully Added Item");

		}
		else
		{
			System.out.println("fail :( ");

		}

		//***7 open first item
		//Get name text from link text 
		String ItemName=driver.findElement(By.cssSelector(".items > ul:nth-child(1) > li:nth-child(1) > div:nth-child(1) > a:nth-child(3)")).getText();
		System.out.println(ItemName);
		Thread.sleep(300);
		//Navigate to first item then click on it so id container is visible to the driver
		WebElement link_Home1 =driver.findElement(By.cssSelector("#item-overview > div.items > ul > li:nth-child(1) > div > img"));
		Actions builder1 = new Actions(driver);
		Action mouseOverHome1 = builder1
				.moveToElement(link_Home1)
				.build();
		mouseOverHome1.perform(); 
		Thread.sleep(300);
		driver.findElement(By.xpath("//*[@id='562ov-63']/div[2]")).click();
		Thread.sleep(400);
		String ItemTitle=driver.findElement(By.cssSelector("#page-title")).getText();

		if(ItemName.contains(ItemTitle))
		{
			System.out.println("Success 'Same Item name as the chosen one' ");
		}

		//*******8 verify the total number of screenshots are 4
		Thread.sleep(300);
		Boolean Displayed= driver.findElement(By.cssSelector("#gallery_nav > a:nth-child(6)")).isDisplayed();
		if(Displayed)
		{
			System.out.println("Forth sceenshoot is displayed properly so the count is 4 screenshots");
		}
		else
		{
			System.out.println("Failed :(( ");
		}
		//***********9Choose quantity 2 from drop down list 	
		Thread.sleep(300);
		WebElement Quantity=driver.findElement(By.cssSelector("#AddToCartForm > div:nth-child(14) > select:nth-child(1)"));
		Select ab4= new Select(Quantity);
		ab4.selectByVisibleText("2");

		//***********10 Add the game to the cart and verify item added.
		Thread.sleep(300); 
		driver.findElement(By.cssSelector("#AddToCartForm > div:nth-child(14) > a:nth-child(2")).click(); 
		Thread.sleep(50);
		String ShoppingChart1=driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/div[4]/div/div[2]/div[1]/div/a[2]")).getText();
		System.out.println(ShoppingChart1);
		//Trim caught text based one their index to catch needed combo. of chars 
		String totalMoney1 = ShoppingChart.substring(1, 6);
		String quantity1 = ShoppingChart.substring(16, 18);
		//Print data extracted and check i data extracted properly
		System.out.println("Total money :"+totalMoney1);
		System.out.println("Items Quantity :"+quantity1);	
		if(quantity1!= null)
		{
			System.out.println("Successfully Added Item");

		}
		else
		{
			System.out.println("fail :( ");

		}
		//*********11 Open shopping charts list
		//OPEN IT
		//Verify that the added items there ,Verify subtotal is the total price of the added items.
		driver.findElement(By.cssSelector(".shopping-cart-title")).click();
		Thread.sleep(500); 
		String ShoppingChartTotal=driver.findElement(By.cssSelector("#cost-overview > table:nth-child(1) > tbody:nth-child(1)")).getText();
		System.out.println("Total Recepit : "+ShoppingChartTotal);
		//************12 Proceed to checkout as a guest.
		//Checkout as aguest
		Thread.sleep(200);
		driver.findElement(By.id("continue-button")).click();
		Thread.sleep(400);
		//Guest user
		driver.findElement(By.cssSelector(".sign-in-column-right > div:nth-child(3) > a:nth-child(1)")).click();
		Thread.sleep(400);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.radio-button:nth-child(2) > input:nth-child(1)")));
		driver.findElement(By.cssSelector("span.radio-button:nth-child(2) > input:nth-child(1)")).click();
		Thread.sleep(200);

		//Create unique random ID to add in the sent data that must unique
		String uniqueID = UUID.randomUUID().toString();
	    Thread.sleep(300);
	    //*************13 Fill the data. and check for validation and then continue filling form and proceed
	    for(int x = 0 ;x<2;x++)
	    {
		try {

		if(x==0){
			
		driver.findElement(By.id("firstName")).sendKeys("First Name");
		Thread.sleep(200);

		driver.findElement(By.id("lastName")).sendKeys("Last Name");
		Thread.sleep(200);
		driver.findElement(By.id("datepicker")).sendKeys("15/12/1993");
		Thread.sleep(200);
		driver.findElement(By.id("emailAddr")).click();
		Thread.sleep(200);
		driver.findElement(By.id("emailAddr")).sendKeys("Test");
		Thread.sleep(200);

		driver.findElement(By.id("streetAddress")).sendKeys("11");

		Thread.sleep(200);
		driver.findElement(By.id("username")).sendKeys(uniqueID);
		Thread.sleep(200);

		driver.findElement(By.id("postcode")).sendKeys("99501");
		Thread.sleep(200);

		driver.findElement(By.id("city")).sendKeys("Cairo");
		Thread.sleep(200);
		WebElement Quantity3=driver.findElement(By.cssSelector("#countryId"));
		Select ab3= new Select(Quantity3);
		ab3.selectByVisibleText("Egypt");
		Thread.sleep(200);
		/*WebElement Quantity1=driver.findElement(By.cssSelector("#state"));
		Select ab2= new Select(Quantity1);
		ab2.selectByVisibleText("Alaska");
		*/
		driver.findElement(By.cssSelector("#state")).sendKeys("Cairo");
		Thread.sleep(200);

		driver.findElement(By.id("telephoneNumber")).sendKeys("+201124118573");
		Thread.sleep(200);
		}
		else
		{
			driver.findElement(By.id("emailAddr")).clear();
			Thread.sleep(200);
			driver.findElement(By.id("emailAddr")).sendKeys(uniqueID+"Test.test@gmail.com");
			Thread.sleep(200);
			driver.findElement(By.id("streetAddress")).sendKeys(uniqueID);
		}


		//confirm the order
		Thread.sleep(500);
		driver.findElement(By.id("continue-button")).click();

		Thread.sleep(200);

		//If input data isn't valid the extraction of error msg while execute
		//Change data of 'sendKeys' to be invalid manually to test the method
		
		} catch (Exception e) {
			String validation=driver.findElement(By.cssSelector("#valid-address > div.form-section-title")).getText();
			System.out.println(validation);
			Thread.sleep(200);
			String validation2=driver.findElement(By.cssSelector("//*[@id='valid-address']/div[2]")).getText();
			System.out.println(validation2);		}
		
	    }
	    Thread.sleep(300);
	    //Print out Bill details
	    String Bill=driver.findElement(By.cssSelector("#formattedDeliveryAddr")).getText();
		System.out.println("Bill details : "+Bill);
        //************14 Change the shipping method and verify the shipping price is changed.
		Thread.sleep(300);
		WebElement Quantity1=driver.findElement(By.cssSelector("#shippingQuotes"));
		Select ab41= new Select(Quantity1);
		ab41.selectByVisibleText("Table Rate");
		Thread.sleep(300);
		String shippingMethod=driver.findElement(By.cssSelector("#ot-table > tr:nth-child(4)")).getText();
		System.out.println("Shipping method details are : "+shippingMethod);
		//**********15 Confirm order.
		driver.findElement(By.cssSelector("#continue-button")).click();
		Thread.sleep(300);
		//*********16 Verify the order has been proceeded.
		String success=driver.findElement(By.cssSelector("#form1 > div.form-section > div")).getText();
		System.out.println("Processing message extraction : "+success);

		Thread.sleep(300);
		driver.findElement(By.cssSelector("#continue-button")).click();
		Thread.sleep(200);
		//**************17 Verify that the order status is pending ins “My account” page.
	    String Status= driver.findElement(By.cssSelector("#last-orders > div.last-order > table > tbody:nth-child(1) > tr > td:nth-child(4) > div")).getText();
		System.out.println("Request status is now : "+Status);
		Thread.sleep(200);
		//*************18 Verify that the price and the name of each item is the same as added before.
		String BillInformation= driver.findElement(By.cssSelector("#last-orders > div.last-order > table > tbody:nth-child(2) > tr > td")).getText();
		System.out.println("Bill Information are  : "+BillInformation);
		Thread.sleep(300);
		System.out.println("Test sucesss");

	}

	//Automated taking screenshot once failure as an  if something expectedly happens
	//Save screenshots using method name and any other declarative methods in pic name and save in 'error screenshots' folder
	@org.testng.annotations.AfterMethod 
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException, InterruptedException { 
		if (testResult.getStatus() == ITestResult.FAILURE) { 
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
					+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
		}

		Thread.sleep(1000);
	}
	//close driver
	@org.testng.annotations.AfterTest

	public void End(){

		driver.close();
	}
   

}